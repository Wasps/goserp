package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

const arraySize = 100
const generations = 31

// rules to live by. Allows a sierpinsky triangle
var rules = [8]int{0, 1, 0, 1, 1, 0, 1, 0}

var grid [arraySize]int

func main() {
	grid = populateGrid(grid)
	//grid = randomize(grid)
	show(grid)

	for i := 0; i < generations; i++ {

		grid = nextGen(grid)
		show(grid)
		time.Sleep(10 * time.Millisecond)
	}

}

func show(array [arraySize]int) {
	for i := 0; i < arraySize; i++ {

		if array[i] == 0 {
			fmt.Print(" ")
		} else {
			fmt.Print("^")
		}

	}
	fmt.Println()

}

func populateGrid(array [arraySize]int) [arraySize]int {
	// all zeros but in the middle
	array[(len(array) / 2)] = 1

	return array
}

func randomize(array [arraySize]int) [arraySize]int {

	rand.Seed(time.Now().UnixNano())
	for i := 0; i < len(array); i++ {
		var rand int = rand.Intn(100)
		if rand >= 95 {
			array[i] = 1
		} else {
			array[i] = 0
		}
		//array[i] = rand.Intn(2)
	}

	return array
}

func nextGen(array [arraySize]int) [arraySize]int {

	var left int
	var middle int
	var right int
	var newArray = array
	for i := 1; i < len(array)-1; i++ {

		left = array[i-1]
		middle = array[i]
		right = array[i+1]

		var ruleNumber int = getRuleNumber(left, middle, right)
		var appliedRule = fetchRule(rules, ruleNumber)
		newArray[i] = appliedRule
	}

	return newArray
}

func fetchRule(ruleArray [8]int, ruleNumber int) int {

	return ruleArray[ruleNumber]
}

func getRuleNumber(left, middle, right int) int {

	// concatenate to make it a binary like string and then convert to integer.
	// this transforms an array of 3 ints into its equivalent in integer value
	var s string = "" + strconv.Itoa(left) + strconv.Itoa(middle) + strconv.Itoa(right)
	i, err := strconv.ParseInt(s, 2, 32)
	if err != nil {
		fmt.Println(err)
	}
	// parseInt return an int64, so it's needed to reconvert into an int
	return int(i)
}
